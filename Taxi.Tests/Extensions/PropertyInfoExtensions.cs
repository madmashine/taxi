﻿using System.Linq;
using System.Reflection;

namespace Taxi.Tests.Extensions
{
    internal static class PropertyInfoExtensions
    {
        #region Methods
        public static T GetTypedAttribute<T>(this PropertyInfo context)
        {
            var attribute = context
                .GetCustomAttributes(typeof(T), false)
                .Cast<T>()
                .FirstOrDefault();

            return attribute;
        } 
        #endregion
    }
}
