using System.ComponentModel.DataAnnotations;
using System.Reflection;
using Taxi.Tests.Extensions;
using Xunit;

namespace Taxi.Tests.CreateCompanyRequestContext.Properties
{
    public class NameAnnotationTests
        : IClassFixture<Taxi.CreateCompanyRequestContext>
    {
        #region Variables
        private readonly PropertyInfo _testingProperty;
        #endregion
        #region Constructors
        public NameAnnotationTests(Taxi.CreateCompanyRequestContext testingContext)
        {
            var testingContextType = testingContext.GetType();

            _testingProperty = testingContextType.GetProperty(nameof(Taxi.CreateCompanyRequestContext.Name));
        }
        #endregion
        #region Tests
        [Fact]
        public void HasRequired()
        {
            var attribute = _testingProperty.GetTypedAttribute<RequiredAttribute>();

            Assert.NotNull(attribute);
        }

        [Fact]
        public void HasStringLength()
        {
            var attribute = _testingProperty.GetTypedAttribute<StringLengthAttribute>();

            Assert.NotNull(attribute);
        }

        [Fact]
        public void StringLength_HasLowerBoundry_3()
        {
            var attribute = _testingProperty.GetTypedAttribute<StringLengthAttribute>();

            Assert.Equal(3, attribute?.MinimumLength);
        }

        [Fact]
        public void StringLength_HasUpperBoundry_20()
        {
            var attribute = _testingProperty.GetTypedAttribute<StringLengthAttribute>();

            Assert.Equal(20, attribute?.MaximumLength);
        }
        #endregion
    }
}
