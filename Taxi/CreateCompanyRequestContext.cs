﻿using System.ComponentModel.DataAnnotations;

namespace Taxi
{
    public class CreateCompanyRequestContext
    {
        #region Variables
        [Required, StringLength(20, MinimumLength = 3)]
        public string Name { get; set; } 
        #endregion
    }
}